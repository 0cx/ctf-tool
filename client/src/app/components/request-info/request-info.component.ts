import {Component, Input, OnInit} from '@angular/core';
import {Request, REQUEST_META_FIELDS} from "../../models/Request";
import {KeyValue} from "@angular/common";

@Component({
    selector: 'app-request-info',
    templateUrl: './request-info.component.html',
    styleUrls: ['./request-info.component.scss']
})
export class RequestInfoComponent implements OnInit {

    constructor() {
    }

    displayedColumns = ['name', 'value'];

    _request: Request = null;
    requestHeaders: Array<KeyValue<string, string>> = null;



    @Input()
    set request(req: Request) {
        this._request = req;
        this.requestHeaders = this.getHeadersArray(req);
    }

    ngOnInit(): void {
    }

    getHeadersArray(req: Request): Array<KeyValue<string, string>> {
        if(req == null || req.headers == null ) return null;
        return Object.entries(req.headers)
            .map(entry => ({
            key: entry[0],
            value: entry[1]
        })).filter(entry => !REQUEST_META_FIELDS.includes(entry.key));
    }
}
