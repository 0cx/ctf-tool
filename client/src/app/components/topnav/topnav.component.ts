import { Component, OnInit } from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";

@Component({
  selector: 'app-topnav',
  templateUrl: './topnav.component.html',
  styleUrls: ['./topnav.component.scss']
})
export class TopnavComponent implements OnInit {

  sidebarClass: string = 'sidebar-hidden';

  constructor(public router: Router) {
    this.router.events.subscribe(val => {
      if (val instanceof NavigationEnd && window.innerWidth <= 992 && this.isToggled()) {
        this.toggleSidebar();
      }
    });
  }

  ngOnInit() {}

  isToggled(): boolean {
    const dom: Element = document.querySelector('body');
    return dom.classList.contains(this.sidebarClass);
  }

  toggleSidebar() {
    console.log("toggle Sidebar");
    const dom: any = document.querySelector('body');
    dom.classList.toggle(this.sidebarClass);
  }

}
