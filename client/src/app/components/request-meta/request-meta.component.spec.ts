import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestMetaComponent } from './request-meta.component';

describe('RequestMetaComponent', () => {
  let component: RequestMetaComponent;
  let fixture: ComponentFixture<RequestMetaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestMetaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestMetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
