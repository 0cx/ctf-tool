import {Component, Input, OnInit} from '@angular/core';
import {Request} from "../../models/Request";

@Component({
  selector: 'app-request-meta',
  templateUrl: './request-meta.component.html',
  styleUrls: ['./request-meta.component.scss']
})
export class RequestMetaComponent implements OnInit {

  @Input()
  request: Request = null;


  constructor() { }

  ngOnInit(): void {
  }

}
