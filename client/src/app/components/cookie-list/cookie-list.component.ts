import {Component, Input, OnInit} from '@angular/core';
import {Request} from "../../models/Request";
import {Cookie} from "../../models/Cookie";

@Component({
    selector: 'app-cookie-list',
    templateUrl: './cookie-list.component.html',
    styleUrls: ['./cookie-list.component.scss']
})
export class CookieListComponent implements OnInit {

    displayedColumns: Array<string> = [];
    cookies: Array<Cookie> = [];

    @Input()
    set request(req: Request) {
        if (req == null || req.cookies == null) {
            this.cookies = [];
        } else {
            this.displayedColumns = this.setColumnDefinitions(req.cookies);
            this.cookies = req.cookies;
            console.log(this.cookies);
        }
    }

    constructor() {
    }

    ngOnInit(): void {
    }

    setColumnDefinitions(cookies: Array<Cookie>): Array<string> {
        const containsField: Map<string, boolean> = new Map();
        for (const c of cookies) {
            Object.entries(c)
                .map(entry => ({key: entry[0], value: entry[1]}))
                .filter(entry => entry.key != 'id')
                .map(entry => {
                    containsField.set(entry.key, entry.value != null);
                });
        }
        return Array.from(containsField.entries())
            .map(entry => ({key: entry[0], value: entry[1]}))
            .filter(entry => entry.value)
            .map(value => value.key);
    }
}
