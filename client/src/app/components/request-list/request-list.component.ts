import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Request} from "../../models/Request";

@Component({
    selector: 'app-request-list',
    templateUrl: './request-list.component.html',
    styleUrls: ['./request-list.component.scss']
})
export class RequestListComponent implements OnInit {

    @Input()
    requests: Request[];
    @Output()
    selectedRequest: EventEmitter<Request> = new EventEmitter<Request>();
    @Output()
    deleteRequest: EventEmitter<Request> = new EventEmitter<Request>();

    selectedId: number;
    displayedColumns = ['method', 'url', 'origin', 'time', 'action'];

    constructor() {
    }

    ngOnInit(): void {
    }

    onSelect(req: Request) {
        this.selectedId = req.id;
        if (this.selectedRequest) this.selectedRequest.emit(req);
    }

    onDelete(event: Event,req: Request) {
        if (this.deleteRequest) this.deleteRequest.emit(req);
        event.stopPropagation();
    }
}
