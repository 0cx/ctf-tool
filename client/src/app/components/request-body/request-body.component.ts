import {Component, Input, OnInit} from '@angular/core';
import {Request, RequestBody} from "../../models/Request";

@Component({
    selector: 'app-request-body',
    templateUrl: './request-body.component.html',
    styleUrls: ['./request-body.component.scss']
})
export class RequestBodyComponent {

    // TODO set language dynamically {language: 'javascript'};
    editorOptions = {theme: 'vs-dark', readOnly: true};

    body: string = null;

    @Input()
    set request(req: Request) {
        if (req == null || req.body == null) {
            this.body = null;
        } else {
            this.body = decodeURI(atob(req.body.content));
        }
    }

    constructor() {
    }

}
