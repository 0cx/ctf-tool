import {Component, OnInit} from '@angular/core';
import {BinService} from "../../services/bin.service";

@Component({
    selector: 'app-debug',
    templateUrl: './debug.page.html',
    styleUrls: ['./debug.page.scss'],
})
export class DebugPage implements OnInit {


    constructor(private binService: BinService) {
    }

    ngOnInit(): void {
    }


}
