import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';


import {MaterialModule} from "../../modules/cdk.module";
import {DebugPageRoutingModule} from "./debug-routing.module";
import {DebugPage} from "./debug.page";

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
      DebugPageRoutingModule,
  ],
  declarations: [
      DebugPage,
  ]
})
export class DebugPageModule {}
