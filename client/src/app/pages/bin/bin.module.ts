import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {BinPageRoutingModule} from './bin-routing.module';

import {BinPage} from './bin.page';
import {MaterialModule} from "../../modules/cdk.module";
import {RequestListComponent} from "../../components/request-list/request-list.component";
import {RequestInfoComponent} from "../../components/request-info/request-info.component";
import {RequestMetaComponent} from "../../components/request-meta/request-meta.component";
import {CookieListComponent} from "../../components/cookie-list/cookie-list.component";
import {RequestBodyComponent} from "../../components/request-body/request-body.component";

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        ReactiveFormsModule,
        BinPageRoutingModule,
    ],
    declarations: [
        BinPage,
        RequestListComponent,
        RequestInfoComponent,
        RequestMetaComponent,
        CookieListComponent,
        RequestBodyComponent,
    ]
})
export class BinPageModule {
}
