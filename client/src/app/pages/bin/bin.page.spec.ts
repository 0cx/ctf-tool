import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BinPage } from './bin.page';

describe('BinPage', () => {
  let component: BinPage;
  let fixture: ComponentFixture<BinPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BinPage ],
    }).compileComponents();

    fixture = TestBed.createComponent(BinPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
