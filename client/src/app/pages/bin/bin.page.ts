import {Component, OnInit} from '@angular/core';
import {BinService} from "../../services/bin.service";
import {Request} from "../../models/Request";

@Component({
    selector: 'app-bin',
    templateUrl: './bin.page.html',
    styleUrls: ['./bin.page.scss'],
})
export class BinPage implements OnInit {

    requests: Request[];

    selectedRequest: Request = null;


    constructor(private binService: BinService) {
    }

    async ngOnInit() {
        this.requests = await this.binService.getAll();
    }

    async onDelete(req: Request) {
        console.log("delete");
        console.log("this.selectedRequest",this.selectedRequest);
        if (this.selectedRequest && this.selectedRequest.id == req.id) this.selectedRequest = null;
        console.log("this.selectedRequest",this.selectedRequest);
        await this.binService.delete(req);
        this.requests = this.requests.filter(e => e.id != req.id);
    }

    onSelect(req: Request) {
        this.selectedRequest = req;
        console.log("this.selectedRequest",this.selectedRequest);
    }
}
