import {HttpClient} from "@angular/common/http";
import {Injectable} from '@angular/core';
import {environment as env} from '../../environments/environment';
import {Request} from "../models/Request";


@Injectable({
    providedIn: 'root'
})
export class BinService {

    constructor(private http: HttpClient) {
    }

    async getAll(): Promise<Request[]> {
        return this.http.get<Request[]>(env.host + env.apiPath + '/bin').toPromise();
    }

    async delete(req: Request): Promise<void> {
        return this.http.delete<void>(env.host + env.apiPath  + '/bin/' + req.id).toPromise();
    }
}
