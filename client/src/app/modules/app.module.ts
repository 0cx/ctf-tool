import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClientModule} from "@angular/common/http";
import {SidebarComponent} from "../components/sidebar/sidebar.component";
import {AppComponent} from "../app.component";
import {TopnavComponent} from "../components/topnav/topnav.component";
import {MaterialModule} from "./cdk.module";

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    TopnavComponent,
  ],
  imports: [
    MaterialModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
