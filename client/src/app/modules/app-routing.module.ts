import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'bin',
        pathMatch: 'full'
    },
    {
        path: 'bin',
        loadChildren: () => import('../pages/bin/bin.module').then(m => m.BinPageModule)
    },
    {
        path: 'debug',
        loadChildren: () => import('../pages/debug/debug.module').then(m => m.DebugPageModule)
    },

];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
