import {Cookie} from "./Cookie";

export class Request {
    id: number = null
    url: string = null
    method: string = null
    headers: { [key: string]: string; } = null
    trailerFields: { [key: string]: string; } = null
    protocol: string = null
    cookies: Array<Cookie> = null
    body: RequestBody = null
    time: Date = null
    size: number = null
    originIp: string = null
    originHost: string = null
}

export class RequestBody {
    id: number = null
    type: string = null
    content: string = null
    size: number = null
}

/**
 * Meta fields of a request should not be listed in Headers Table.
 * They have a special section in the meta component.
 */
export const REQUEST_META_FIELDS = ['cookie']