export class Cookie {
     name: string
     value: string = null
     comment: string = null
     domain: string = null
     maxAge: number = null
     path: string = null
     secure: boolean = false
     httpOnly: boolean = false
}