import com.moowork.gradle.node.npm.NpmTask
import com.moowork.gradle.node.task.NodeTask

group = "de.xc0.client"

plugins {
    java
    id("com.github.node-gradle.node") version "2.2.4"
}

java.sourceCompatibility = JavaVersion.VERSION_13


tasks.jar {
    dependsOn("ngBuild")
    from("dist/client")
    into("static")
}

node {
    version = "15.0.0"
    download = true
}

tasks.register("ngBuild", NodeTask::class) {
    group = LifecycleBasePlugin.BUILD_GROUP
    dependsOn("npmInstall")

    inputs.files("package.json", "package-lock.json", "angular.json", "tsconfig.json", "tsconfig.app.json")
    inputs.dir("src")
    inputs.dir(fileTree("node_modules").exclude(".cache"))

    outputs.dir("dist/")
    script = file("./node_modules/@angular/cli/bin/ng")
    setArgs(listOf("build", "--prod"))
}


tasks.register("ngTest", NpmTask::class) {
    group = LifecycleBasePlugin.VERIFICATION_GROUP
    dependsOn("npmInstall")

    setArgs(listOf("run", "test"))
}


tasks.getByName("processResources").dependsOn("ngBuild")
//tasks.getByName("test").dependsOn("ngTest")


tasks.getByName("clean").doLast {
    delete("node_modules")
    delete("coverage")
    delete("documentation")
    delete("dist")
}