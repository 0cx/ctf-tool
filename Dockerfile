FROM openjdk:14-alpine

COPY build/libs/ctf-tool.jar /ctf-tool.jar

CMD ["java", "-jar" , "/ctf-tool.jar"]