package de.xc0.ctftool.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.servlet.http.Cookie


@Entity
data class CookieEntity(
        @Id
        @GeneratedValue
        val id: Long?,
        val name: String,
        val value: String? = null,
        val comment: String? = null,
        val domain: String? = null,
        val maxAge: Int? = null,
        val path: String? = null,
        val secure: Boolean = false,
        val httpOnly: Boolean = false
)