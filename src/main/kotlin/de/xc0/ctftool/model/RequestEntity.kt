package de.xc0.ctftool.model

import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.format.annotation.DateTimeFormat
import java.time.ZonedDateTime
import javax.persistence.*

@Entity
data class RequestEntity(
        @Id
        @GeneratedValue
        val id: Long?,
        val url: String?,
        val method: String?,
        @ElementCollection
        @Column(columnDefinition = "TEXT")
        val headers: Map<String, String>?,
        @ElementCollection
        @Column(columnDefinition = "TEXT")
        val trailerFields: Map<String, String>?,
        val protocol: String?,
        @OneToMany(cascade = [CascadeType.ALL])
        val cookies: List<CookieEntity>?,
        @OneToOne(cascade = [CascadeType.ALL])
        val body: RequestBodyEntity?,
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
        val time: ZonedDateTime,
        val size: Int,
        val originIp: String,
        val originHost: String
)