package de.xc0.ctftool.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Lob

@Suppress("ArrayInDataClass")
@Entity
data class RequestBodyEntity(
        @Id
        @GeneratedValue
        val id: Long?,
        val type: RequestBodyContentType,
        @Lob
        val content: ByteArray?,
        val size: Int
)

