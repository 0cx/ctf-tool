package de.xc0.ctftool.model

enum class RequestBodyContentType {
        TEXT,
        BINARY,
}