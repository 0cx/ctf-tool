package de.xc0.ctftool.controller

import de.xc0.ctftool.model.RequestEntity
import de.xc0.ctftool.repository.RequestRepository
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.time.ZonedDateTime

@RestController
@RequestMapping("/api/bin")
class BinController(val requestRepository: RequestRepository) {

    private val log = LoggerFactory.getLogger(javaClass)

    @GetMapping
    fun list(): MutableList<RequestEntity> {
        log.debug("enter list()")
        return requestRepository.findAll();
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long) {
        requestRepository.deleteById(id);
    }
}