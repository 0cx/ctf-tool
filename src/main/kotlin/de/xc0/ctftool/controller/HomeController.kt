package de.xc0.ctftool.controller

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
class HomeController {

    private val log = LoggerFactory.getLogger(javaClass)

    @GetMapping("/")
    fun index(): String {
        return "/index.html";
    }
}
