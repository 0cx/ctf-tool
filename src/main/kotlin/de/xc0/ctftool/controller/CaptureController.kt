package de.xc0.ctftool.controller

import de.xc0.ctftool.annotation.CaptureRequests
import de.xc0.ctftool.model.CookieEntity
import de.xc0.ctftool.model.RequestBodyEntity
import de.xc0.ctftool.model.RequestEntity
import de.xc0.ctftool.repository.RequestRepository
import de.xc0.ctftool.service.encoder.BinaryHandler
import de.xc0.ctftool.service.encoder.RequestBodyHandler
import de.xc0.ctftool.service.encoder.TextHandler
import de.xc0.ctftool.service.encoder.UrlEncodedHandler
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.time.ZonedDateTime
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest

@Controller
class CaptureController(val requestRepository: RequestRepository) {

    private val log = LoggerFactory.getLogger(javaClass)

    @CaptureRequests
    fun capture(
            @RequestAttribute param: String?,
            @RequestHeader headers: Map<String, String>,
            @RequestParam(required = false) files: Array<MultipartFile>?,
            @RequestBody(required = false) body: ByteArray?,
            request: HttpServletRequest
    ): ResponseEntity<Void> {
        val contentType = headers[HttpHeaders.CONTENT_TYPE.toLowerCase()];
        val requestEntity = RequestEntity(
                null,
                request.requestURI.toString(),
                request.method,
                headers,
                request.trailerFields,
                request.protocol,
                request.cookies?.map { it.toEntity() },
                createRequestBody(contentType, body),
                ZonedDateTime.now(),
                1,
                request.remoteAddr,
                request.remoteHost)
        requestRepository.save(requestEntity)
        return ResponseEntity.ok().build()
    }

    fun Cookie.toEntity() = CookieEntity(
            null,
            name,
            value,
            comment,
            domain,
            if (maxAge == -1) null else maxAge,
            path,
            secure,
            isHttpOnly
    )

    fun createRequestBody(contentType: String?, body: ByteArray?): RequestBodyEntity? {
        if (body == null) return null

        val bodyHandler: RequestBodyHandler = when (contentType) {
            // TODO different file types
            // TODO BinaryEncoder -> FileTypeDetectionEncoder
            MediaType.APPLICATION_FORM_URLENCODED_VALUE -> UrlEncodedHandler()
            MediaType.TEXT_HTML_VALUE -> TextHandler()
            else -> BinaryHandler()
        }
        return bodyHandler.encode(body)
    }
}