package de.xc0.ctftool.repository

import de.xc0.ctftool.model.RequestEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RequestRepository : JpaRepository<RequestEntity, Long?>