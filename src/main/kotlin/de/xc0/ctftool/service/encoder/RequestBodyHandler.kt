package de.xc0.ctftool.service.encoder

import de.xc0.ctftool.model.RequestBodyEntity

interface RequestBodyHandler {

    fun encode(body: ByteArray): RequestBodyEntity

}