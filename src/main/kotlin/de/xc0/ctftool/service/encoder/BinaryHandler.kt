package de.xc0.ctftool.service.encoder

import de.xc0.ctftool.model.RequestBodyContentType
import de.xc0.ctftool.model.RequestBodyEntity

class BinaryHandler : RequestBodyHandler {
    override fun encode(body: ByteArray): RequestBodyEntity {
        return RequestBodyEntity(
                null,
                RequestBodyContentType.BINARY,
                body,
                body.size
        )
    }
}