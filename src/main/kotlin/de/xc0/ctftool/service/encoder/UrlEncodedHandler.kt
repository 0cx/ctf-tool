package de.xc0.ctftool.service.encoder

import de.xc0.ctftool.model.RequestBodyContentType
import de.xc0.ctftool.model.RequestBodyEntity
import java.nio.charset.StandardCharsets

class UrlEncodedHandler : RequestBodyHandler {
    override fun encode(body: ByteArray): RequestBodyEntity {
        val decodedBody = java.net.URLDecoder.decode(String(body), StandardCharsets.UTF_8).toByteArray()

        return RequestBodyEntity(
                null,
                RequestBodyContentType.BINARY,
                decodedBody,
                body.size
        )
    }
}