package de.xc0.ctftool.service.encoder

import de.xc0.ctftool.model.RequestBodyContentType
import de.xc0.ctftool.model.RequestBodyEntity

class TextHandler : RequestBodyHandler {
    override fun encode(body: ByteArray): RequestBodyEntity {
        return RequestBodyEntity(
                null,
                RequestBodyContentType.TEXT,
                body,
                body.size
        )
    }
}