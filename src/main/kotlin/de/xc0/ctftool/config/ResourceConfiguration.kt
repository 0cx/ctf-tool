package de.xc0.ctftool.config

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.core.io.support.PathMatchingResourcePatternResolver
import org.springframework.core.io.support.ResourcePatternResolver
import org.springframework.http.CacheControl
import org.springframework.web.servlet.ViewResolver
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistration
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.view.InternalResourceViewResolver
import java.util.concurrent.TimeUnit
import org.springframework.core.io.Resource


@Configuration
class ResourceConfiguration : WebMvcConfigurer {

    private val log = LoggerFactory.getLogger(javaClass)

    companion object {
        val RESOURCE_LOCATIONS = arrayOf("classpath:/static/")
        val cacheControl: CacheControl = CacheControl.maxAge(1, TimeUnit.DAYS).cachePublic()
    }

    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        val resourceHandlerRegistration: ResourceHandlerRegistration = appendResourceHandler(registry)
        initializeResourceHandler(resourceHandlerRegistration)
    }

    /**
     * removes suffix of .html/.jsp
     */
    @Bean
    fun getViewResolver(): ViewResolver? {
        val resolver = InternalResourceViewResolver()
        resolver.setSuffix("")
        resolver.order = Ordered.HIGHEST_PRECEDENCE
        return resolver
    }

    override fun configureDefaultServletHandling(configurer: DefaultServletHandlerConfigurer) {
        super.configureDefaultServletHandling(configurer)
        configurer.enable()
    }

    fun appendResourceHandler(registry: ResourceHandlerRegistry): ResourceHandlerRegistration {
        val cl = this.javaClass.classLoader
        val resolver: ResourcePatternResolver = PathMatchingResourcePatternResolver(cl)
        val staticResources = resolver.getResources("classpath:/static/**")
                .filter { resource -> !(resource.filename?.equals("MANIFEST.MF") ?: false) }
                .filter { resource -> !(resource.filename?.endsWith("/") ?: false) }
                .filter { resource -> !(resource.filename?.isEmpty() ?: false) }
                .map { resource -> "/${resource.filename}" }

        registry.setOrder(Ordered.HIGHEST_PRECEDENCE)
        return registry.addResourceHandler(*staticResources.toTypedArray())
    }

    fun initializeResourceHandler(resourceHandlerRegistration: ResourceHandlerRegistration) {
        resourceHandlerRegistration
                .addResourceLocations(*RESOURCE_LOCATIONS)
                .setCacheControl(cacheControl)
    }
}
