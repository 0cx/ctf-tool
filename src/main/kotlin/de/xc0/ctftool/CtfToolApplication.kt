package de.xc0.ctftool

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CtfToolApplication

fun main(args: Array<String>) {
	runApplication<CtfToolApplication>(*args)
}
